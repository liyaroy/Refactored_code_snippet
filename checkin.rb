class Checkin < ActiveRecord::Base
  include Checkins::CheckinExtension
  def notification(employee, method_name, delete_description = '')
    @emp_name = employee.emp_name
    @delete_description = delete_description
    hash = send("notification_#{method_name}_hash")
    email_params = { subject: hash[:subject], content: hash[:content], 
                     link: hash[:link] }
    fcm_params = { event: hash[:event], title: hash[:title],
                   assigned_employee_id: "#{assigned_employees.map(&:id)[0]}" }
    recipients = find_recipients(method_name, employee)
    notify(recipients, email_params, fcm_params)
  end

  def notification_create_hash
    { subject: "#{@emp_name} created checkin ##{id}",
      content: "#{@emp_name} a checkin ##{id}",
      link: true, event: 'create',
      title: "#{@emp_name.truncate(10)} created a new checkin" }
  end 

  def notification_update_hash
     { subject: "#{@emp_name} edited checkin ##{id}",
       content: "#{@emp_name} edited checkin ##{id}",
       link: true, event: 'update',
       title: "#{@emp_name.truncate(10)} updated the checkin" } 
  end

  def notification_import_hash
    { subject: "Created the checkin ##{id} for #{@emp_name}",
      content: "Created the checkin ##{id} for #{@emp_name}",
      link: true, event: 'update',
      title: "#checkin created for {@emp_name.truncate(10)}" }
  end

  def notification_delete_hash
    { subject: "#{@emp_name} deleted the checkin ##{id}",
      content: "#{@emp_name} says:  " + "'#{@delete_description}'",
      link: false, event: 'delete',
      title: "#{@emp_name.truncate(10)} deleted the checkin" }
    }
  end

  def notification_overdue_hash
    { subject: "Checkin is overdue - #{@emp_name}",
      content: "Checkin ##{id} - #{name} - assigned to #{@emp_name} is overdue!",
      link: true, event: 'checkin_overdue',
      title: "Checkin overdue - #{@emp_name.truncate(10)}" }
    }
  end

  def find_recipients(method_name, employee)
    if %w(create update destroy).include?(method_name)
      find_actors(employee)
    elsif method_name == 'overdue'
      [employee]
    else
      [employee, employee.superior].flatten.compact.uniq
    end
  end
end

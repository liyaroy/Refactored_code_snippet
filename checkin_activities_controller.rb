class Api::V1::CheckinActivitiesController < Api::V1::BaseController

  def index
    sanitize_incoming_params(params)
    get_pagination_values(params)
    checkin_activities = []
    checkin = Checkin.where(id: params[:checkin_id]).first
    if checkin
      checkin_activities = checkin.checkin_activities.includes(activity: :employee)
                           .find_records(params)
      set_header(checkin_activities, params)
      params[:checkin] = checkin
      params[:current_employee] = @current_employee
    end
    render json: { server_time: Time.now,
                   checkin_activities: ActiveModel::ArraySerializer.new(checkin_activities,
                                     each_serializer: CheckinActivitySerializer, params: params) }
  end

  def set_header(checkin_activities, params)
    if checkin_activities.present?
      if params.has_key?(:page_activity_id)
        set_activity_primary_key_pagination_header(params, checkin_activities.last)
      else
        set_activity_pagination_header(params)
      end
    end
  end
end

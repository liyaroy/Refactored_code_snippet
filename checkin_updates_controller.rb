class CheckinUpdatesController < ApplicationController
  def create
    @checkin = Checkin.where(id: params[:checkin_id]).first
    if @checkin
      params[:previous_progress] = @checkin.progress
      @checkin.assign_attributes(checkin_params)
      @checkin.set_completed_at
      checkin_update = @checkin.checkin_updates.build(checkin_update_params)
      if @checkin.save
        checkin_update.notify_on_create(current_employee)
      end
    end
    respond_to do |format|
      format.js { redirect_to controller: :checkin_activities,
       action: :index, params: params.merge(update_list: true) }
    end
  end

  private

  def checkin_update_params
    params.require(:checkin_update).permit(:description, :progress, :latitude, :longitude).
      merge(employee_id: @current_employee.id, description: params[:description], previous_progress: params[:previous_progress])
  end

  def checkin_params
    params.require(:checkin_update).permit(:progress)
  end

end
